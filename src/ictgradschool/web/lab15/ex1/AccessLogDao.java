package ictgradschool.web.lab15.ex1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccessLogDao {
    private final Connection conn;

    public AccessLogDao() throws SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    public List<AccessLog> logQuery() throws SQLException {
        List<AccessLog> accessLogList = new ArrayList<>();
        try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM lab14_ex_accessLog;")) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    AccessLog a = new AccessLog();
                    a.setId(rs.getInt(1));
                    a.setName(rs.getString(2));
                    a.setDes(rs.getString(3));
                    a.setTime(rs.getTime(4).toString());
                    accessLogList.add(a);
                }
            }
        }
        return accessLogList;
    }

    public void insertLog(String name, String des) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                "INSERT INTO lab14_ex_accessLog(name,des) VALUES (?,?);")) {
            ps.setString(1, name);
            ps.setString(2, des);
            ps.executeUpdate();
        }
    }

    public void deleteLog(int id) throws SQLException {
        try(PreparedStatement ps=conn.prepareStatement(
                "DELETE FROM lab14_ex_accessLog WHERE id=?;")){
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }
}
