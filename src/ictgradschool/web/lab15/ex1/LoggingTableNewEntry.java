package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        String newName = request.getParameter("newname");
        String newDes = request.getParameter("newdes");
        response.setContentType("text/html");

        try {
            AccessLogDao a = new AccessLogDao();
            a.insertLog(newName, newDes);
            pw.println("<form action=\"/lognew\" method=\"post\">\n" +
                    "    <fieldset>\n" +
                    "        <legend>New Entry</legend>\n" +
                    "        Name:\n" +
                    "        <input type=\"text\" name=\"newname\">\n" +
                    "        </br></br>\n" +
                    "        Description:\n" +
                    "        <input type=\"text\" name=\"newdes\">\n" +
                    "        </br></br>\n" +
                    "        <input type=\"submit\" value=\"Submit\">\n" +
                    "    </fieldset>\n" +
                    "</form>");
            List<AccessLog> list = a.logQuery();
            for(AccessLog al:list){
                pw.println("<table>");
                pw.println("<tr>");
                pw.println("<td>"+al.getId()+"</td>");
                pw.println("<td>"+al.getName()+"</td>");
                pw.println("<td>"+al.getDes()+"</td>");
                pw.println("<td>"+al.getTime()+"</td>");
                pw.println("</tr>");
                pw.println("</table>");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
