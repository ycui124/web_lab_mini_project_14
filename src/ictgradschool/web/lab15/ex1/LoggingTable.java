package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;


public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        try {
            AccessLogDao ald = new AccessLogDao();
            List<AccessLog> list = ald.logQuery();
            for (AccessLog a : list) {
                pw.println("<table>");
                pw.println("<tr>");
                pw.println("<td>" + a.getId() + "</td>");
                pw.println("<td>" + a.getName() + "</td>");
                pw.println("<td>" + a.getDes() + "</td>");
                pw.println("<td>" + a.getTime() + "</td>");
                pw.println("</tr>");
                pw.println("</table>");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        pw.close();


    }
}
